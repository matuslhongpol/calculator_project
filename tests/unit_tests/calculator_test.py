import pytest
from src import calculator
# --------------------------

# @pytest.mark.add
@pytest.mark.parametrize('input1,input2,output', [(1, 2, 3), (1, 6, 7)])
def test_add(input1, input2, output):
    assert calculator.add(input1, input2) == output
