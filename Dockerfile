# # Use the official Jenkins LTS image as the base image
# FROM jenkins/jenkins:lts

# # Switch to the root user to install packages
# USER root

# # Install Python and related tools
# RUN apt-get update \
#     && apt-get install -y python3 python3-pip

# # Switch back to the Jenkins user
# USER jenkins

FROM jenkins/jenkins:lts
USER root
RUN apt-get update && apt-get install -y lsb-release
RUN curl -fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
    https://download.docker.com/linux/debian/gpg
RUN echo "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
    https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
RUN apt-get update && apt-get install -y docker-ce-cli
RUN apt-get update && apt-get install -y python3 python3-pip
USER jenkins
RUN jenkins-plugin-cli --plugins "blueocean:1.25.3 docker-workflow:1.28"